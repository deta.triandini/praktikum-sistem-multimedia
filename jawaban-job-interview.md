# No 1 Dokumentasi
![Source code](https://gitlab.com/deta.triandini/sistemmultimedia-deta/-/tree/master?ref_type=heads)
<br>![ScreenRecord](https://gitlab.com/deta.triandini/sistemmultimedia-deta/-/raw/master/Demo-Image-To-Cartoon.gif)

**Tampilan Aplikasi**
1. Aplikasi dijalankan melalui command prompt atau CLI dengan memasukkan perintah "python namaFile.py"
![Image](https://gitlab.com/deta.triandini/sistemmultimedia-deta/-/raw/master/tampilan-1.jfif)
2. Lalu akan ditampilkan file dialog atau pop up windows yang membuka file explorer perangkat kita. 
![image](https://gitlab.com/deta.triandini/sistemmultimedia-deta/-/raw/master/tampilan-2.jfif)
3. User diminta untuk memilih gambar yang akan di konversi menjadi kartun 
4. Hasil konversi gambar yang dipilih
![image](https://gitlab.com/deta.triandini/sistemmultimedia-deta/-/raw/master/tampilan-3.jfif)


# No 2 Cara Kerja 
Produk image yang saya buat adalah aplikasi berbasis desktop/CLI. Aplikasi ini akan mengonversi gambar menjadi kartun dengan memberikan efek kartun pada gambar yang diolah. Untuk pemrosesan citra saya menggunakan library opencv dan numpy dan menggunakan modul file dialog dari library tkinter untuk menampilkan jendela windows pop-up agar dapat memilih gambar yang akan diolah dari file local.

<b> Flowchart Aplikasi Image to Cartoon
```mermaid
    graph TB;
        id1([Start])-->id2[Choose Image in Filedialog];
        id2-->id3[/Input Image to convert/];
        id3-->id4[Change the background color of the original image to grayscale using function cv2.cvtColor];
        id4-->id5[Remove image noise using function cv2.medianBlur];
        id5-->id6[Doing adaptive threshold using cv2.adaptiveThreshold];
        id6-->id7[Apply a bilateral filter using cv2.bilateralFilter];
        id7-->id8[Performs a bitwise AND operation between color and edges images using cv2.bitwise_and];
        id8-->id9[/Output: original image is displayed and the image is given a cartoon effect/];
        id9-->id10([Finish]);
```

<b>Cara Kerja Aplikasi Image to Cartoon

1. Mengimpor pustaka yang diperlukan, yaitu cv2 (OpenCV), numpy, dan tkinter.filedialog.
2. Menggunakan askopenfilename() dari tkinter.filedialog untuk memilih file gambar.
3. Membaca gambar yang dipilih menggunakan cv2.imread() dan menyimpannya dalam variabel img.
4. Mengonversi gambar ke skala keabuan menggunakan cv2.cvtColor() dengan parameter cv2.COLOR_BGR2GRAY dan menyimpannya dalam variabel grey.
5. Mengaburkan gambar skala keabuan menggunakan cv2.medianBlur() dengan parameter grey dan ukuran kernel 5. Hasilnya disimpan kembali dalam variabel grey.
6. Menerapkan deteksi tepi adaptif pada gambar menggunakan cv2.adaptiveThreshold(). Parameter yang digunakan adalah grey sebagai gambar input, 255 sebagai nilai maksimum untuk piksel yang bernilai lebih besar daripada ambang, cv2.ADAPTIVE_THRESH_MEAN_C sebagai metode penghitungan ambang adaptif, cv2.THRESH_BINARY sebagai tipe ambang, dan 9 untuk ukuran blok dan 9 untuk konstanta ambang. Hasilnya disimpan dalam variabel edges.
7. Melakukan filtrasi bilateral pada gambar warna menggunakan cv2.bilateralFilter(). Parameter yang digunakan adalah img sebagai gambar input, 9 sebagai diameter piksel, 250 sebagai rentang intensitas yang akan diperhitungkan, dan 250 sebagai rentang intensitas spasial yang akan diperhitungkan. Hasilnya disimpan dalam variabel color.
8. Melakukan operasi bitwise AND antara gambar warna (color) dan masker (edges) menggunakan cv2.bitwise_and(). Hasilnya disimpan dalam variabel cartoon.
9. Menampilkan gambar asli (img) dan gambar hasil kartunisasi (cartoon) menggunakan cv2.imshow().
10. Menyimpan gambar hasil kartunisasi (cartoon) ke dalam file "cartoon.jpg" menggunakan cv2.imwrite().
11. Menunggu pengguna menekan tombol pada keyboard menggunakan cv2.waitKey(0).
12. Menutup semua jendela gambar menggunakan cv2.destroyAllWindows().
<p>Dengan demikian, program di atas mengambil gambar input, mengubahnya menjadi gambar kartun dengan langkah-langkah pemrosesan gambar yang disebutkan di atas, dan menampilkan gambar asli dan gambar kartun tersebut, serta menyimpan gambar kartun ke dalam file "cartoon.jpg".</p>

<b>Algoritma Konversi Image to Cartoon atau Detail proses pengonversian gambar menjadi kartun
1. User akan diminta untuk memilih gambar yang akan dikonversi menjadi kartun dari file local.
2. Dalam pengonversian gambar menjadi kartun saya menerapkan beberapa teknik pengolahan citra, yaitu :
<ul><li>Saya mengubah background warna dari gambar asli menjadi grayscale.Karena efek kartun biasanya didasarkan pada perbedaan intensitas atau nilai keabuan pada gambar, bukan pada informasi warna. Dengan mengubah gambar ke citra grayscale, kita dapat fokus pada keabuan piksel dan fitur struktural gambar tanpa terpengaruh oleh variasi warna.</li>
<li>Setelah citra diubah menjadi grayscale, beberapa teknik pengolahan citra diterapkan untuk menciptakan efek kartun. Saya menerapkan penggunaan median blur untuk menghaluskan citra dan menghilangkan noise. Noise pada gambar dapat mengganggu proses pemrosesan dan menghasilkan detail yang tidak diinginkan pada hasil akhir. Median blur merupakan metode penghalusan citra yang efektif dalam mengurangi noise, terutama noise jenis salt-and-pepper yang sering terjadi pada gambar.

Metode median blur menggunakan kernel berukuran n x n (dalam kasus ini, ukuran kernel adalah 5x5) untuk mengganti nilai piksel pada suatu titik dengan nilai median dari piksel-piksel yang termasuk dalam kernel tersebut. Dengan demikian, piksel yang terpengaruh oleh noise akan "terhaluskan" dan didominasi oleh piksel-piksel sekitarnya yang lebih representatif.</li>
<li>Lalu diterapkan metode treshold adaptif bertujuan untuk menghasilkan citra biner yang menyoroti tepi atau batas objek pada gambar dengan penyesuaian ambang batas secara lokal.
Metode thresholding konvensional menggunakan nilai ambang batas tunggal yang sama untuk semua piksel pada gambar. Namun, pada gambar yang kompleks atau dengan variasi intensitas yang signifikan, pendekatan ini mungkin tidak memberikan hasil yang baik karena nilai ambang batas yang sama tidak dapat menghasilkan segmentasi yang akurat.

Threshold adaptif mengatasi masalah ini dengan memperhitungkan intensitas piksel di sekitar setiap piksel untuk menentukan ambang batasnya. Metode cv2.adaptiveThreshold() yang digunakan dalam program tersebut mengadopsi pendekatan ambang batas adaptif berdasarkan rata-rata intensitas piksel di sekitarnya.
Pada langkah tersebut, citra grayscale grey digunakan sebagai input. Parameter 255 menentukan nilai maksimum untuk piksel yang melewati ambang batas dan akan diberikan nilai maksimum (putih) pada citra biner yang dihasilkan. cv2.ADAPTIVE_THRESH_MEAN_C menunjukkan bahwa penghitungan ambang batas adaptif didasarkan pada rata-rata intensitas piksel. cv2.THRESH_BINARY menunjukkan bahwa hasilnya akan berupa citra biner dengan dua nilai, hitam dan putih. Parameter 9 dan 9 menentukan ukuran blocksize yang digunakan dalam perhitungan ambang batas dan konstanta yang dikurangkan dari ambang batas yang dihasilkan.</li>
<li>Selanjutnya menerapkan metode bilateral filtering bertujuan untuk menciptakan efek kartun pada gambar dengan mempertahankan detail penting sambil menghaluskan daerah yang kurang penting.
Bilateral filter adalah salah satu metode penghalusan citra yang dapat mengurangi noise dan menghaluskan gambar secara keseluruhan, sambil mempertahankan tepi atau detail penting pada gambar. Bilateral filter menggunakan dua komponen dalam proses penghalusan, yaitu komponen spasial dan komponen intensitas. Dalam penerapannya, parameter pertama adalah gambar input img, yang akan diolah menggunakan bilateral filter. Parameter kedua adalah d, yang menentukan jangkauan spasial, yaitu sejauh mana piksel tetangga akan dipertimbangkan dalam perhitungan penghalusan. Parameter ketiga dan keempat adalah sigmaColor dan sigmaSpace, yang mengontrol pengaruh intensitas dan jarak pada proses penghalusan. Nilai yang lebih besar pada sigmaColor dan sigmaSpace akan menghasilkan penghalusan yang lebih kuat, sedangkan nilai yang lebih kecil akan mempertahankan lebih banyak detail pada gambar.
Dalam konteks pembuatan efek kartun, bilateral filter digunakan untuk menghasilkan citra dengan tampilan yang lebih halus, namun tetap mempertahankan garis-garis dan detail penting dalam gambar. Citra yang telah diolah dengan bilateral filter kemudian digunakan dalam operasi bitwise and dengan citra hasil thresholding sebelumnya untuk membentuk efek kartun pada gambar.</li>
<li>Kemudian menerapkan metode bitwise_and yang digunakan untuk menggabungkan citra hasil bilateral filter dengan citra biner hasil thresholding (edges) sehingga menghasilkan efek kartun yang menyoroti garis-garis atau tepi objek pada gambar. Operasi bitwise AND menggabungkan dua citra dengan mempertahankan piksel yang memiliki nilai 1 pada kedua citra, sementara mengabaikan piksel dengan nilai 0 pada salah satu atau kedua citra. Pada kasus ini, citra warna hasil bilateral filter (color) dijadikan input pada kedua parameter bitwise AND, sementara citra biner hasil thresholding (edges) digunakan sebagai mask atau masker. Dengan menggunakan mask (edges) dalam operasi bitwise AND, hanya piksel pada citra color yang berada pada posisi di mana edges memiliki nilai 1 (putih) yang akan dipertahankan, sementara piksel pada posisi di mana edges memiliki nilai 0 (hitam) akan diabaikan. Dengan demikian, hanya garis-garis atau tepi objek pada gambar yang akan muncul dalam citra hasilnya.</li>

3. Lalu akan ditampilkan gambar hasil konversi dan gambar aslinya menggunakan jendela windows pop-up. Dan kemudian gambar hasil konversi disimpan di file baru dengan nama file cartoon.jpg

Link Youtube: https://youtu.be/igei-KdqAsY

# No 3 Artificial Intelligence
Kecerdasan buatan (Artificial Intelligence/AI) pada aplikasi yang sayabuat tidak secara langsung terlihat. Kode tersebut menggunakan pustaka OpenCV (Open Source Computer Vision) yang menyediakan berbagai fungsi dan algoritma pemrosesan gambar dan komputer visi. Meskipun OpenCV mencakup beberapa algoritma yang digunakan dalam bidang kecerdasan buatan, seperti deteksi objek atau pengenalan wajah, kode di atas tidak secara khusus memanfaatkan algoritma AI tersebut.
Namun demikian, kode tersebut menggunakan algoritma pemrosesan gambar tradisional seperti konversi ke skala keabuan, penajaman tepi (edge detection), dan filtrasi bilateral. 

# No 4 Dokumentasi
![Link Source Code](https://gitlab.com/deta.triandini/sistemmultimedia-deta/-/tree/main?ref_type=heads) 
<br>![screenrecord](https://gitlab.com/deta.triandini/praktikum-sistem-multimedia/-/raw/main/Demo-Baca-Artikel_Otomatis.mp4)
<br>**Tampilan Aplikasi**
1. Aplikasi dijalankan melalui CLI dengan memasukkan perintah "python namaFile.py url"
![image](https://gitlab.com/deta.triandini/sistemmultimedia-deta/-/raw/main/tampilan-1.jfif)
2. Aplikasi akan memproses, dari mulai memparsing web HTML kemudian mencari elemen paragraf lalu menggabungkannya kembali sehingga output dari aplikasi ini berupa file audio yang tersimpan di perangkat kita. (Lebih jelasnya bisa dibaca di penjelasan cara kerja di no selanjutnya)
3.  File audio hasil perubahan dari teks to speech 
![image](https://gitlab.com/deta.triandini/sistemmultimedia-deta/-/raw/main/tampilan-2.jfif)

# No 5 Cara Kerja 
Produk Audio yang saya buat merupakan aplikasi berbasis desktop atau CLI. Aplikasi ini akan membaca artikel pada website secara otomatis dengan mengambil teks dari url artikel yang dimasukkan, kemudian mengubahnya menjadi suara menggunakan mesin TTS yang terkonfigurasi di komputer. Aplikasi ini dibuat menggunakan library gTTS untuk melakukan pemrosesan audio dengan mengubah teks menjadi sinyal suara dan kemudian menjalankan output audio yang dihasilkan untuk memainkannya. Selain itu, saya juga menggunakan library request untuk melakukan permintaan HTTP untuk mengambil konten halaman web dan beautifulsoap untuk memparsing dan mengekstrak data dari HTML.

<b> Flowchart Speech Sythesis Membaca Artikel dari website
```mermaid
    flowchart TB
        id1([Start])-->id2[/Input URL Website/]
        id2-->id3[Retrieving article content from the URL using the request library]
        id3-->id4[Parse the HTML content using the BeautifulSoup library]
        id4-->id5[Look for paragraph elements from the text of the article you want to read]
        id5-->id6{Check the language article from the response header}
        id6-->|NO|id7[Audio output is English as default language]
        id6-->|YES|id8[Audio output according to the language code in the article]
        id7-->id9[Create a gTTS object with the specified text and language]
        id8-->id9
        id9-->id10[Convert text to voice using gTTS objects]
        id10-->id11[/Output: Save the converted audio filename.mp3/]
        id11-->id12([Finish])

```

<b>Flowchart convert text to voice using gTTS
```mermaid
    flowchart TB
        id1([Start])-->id2[Create a gTTS object with the specified text and language]
        id2-->id3[Send a text request to the Google Text-to-Speech API]
        id3-->id4[Receive audio response from API]
        id4-->id5[Reads binary audio files]
        id5-->id6[Convert the audio response to a suitable format. Example: MP3]
        id6-->id7[Save audio to file]
        id7-->id8([Finish])
```
<b>Cara Kerja Speech Synthesis Membaca Artikel Otomatis
1. Program mengimpor modul yang diperlukan, yaitu sys untuk mengambil argumen dari command-line interface (CLI), requests untuk mengirim permintaan HTTP ke halaman web, BeautifulSoup untuk mem-parsing konten halaman web, dan gTTS untuk membuat speech synthesis.
2. Fungsi speech_synthesis_from_web didefinisikan dengan satu parameter url. Fungsi ini bertugas untuk melakukan speech synthesis dari halaman web yang diberikan.
3. Di dalam fungsi speech_synthesis_from_web:
<ul><li>Program menggunakan requests.get(url) untuk mengirim permintaan HTTP GET ke halaman web yang diberikan dan mendapatkan responsenya.</li>
<li>Konten halaman web yang diterima di-parse menggunakan BeautifulSoup(response.content, "html.parser") menjadi objek BeautifulSoup yang dapat diproses lebih lanjut.</li>
<li>Program mencari elemen-elemen teks yang ingin dibaca dari halaman web, dalam contoh ini, program mencari semua elemen paragraf menggunakan soup.find_all("p"). Teks dari setiap elemen yang ditemukan diambil menggunakan .get_text() dan dikumpulkan dalam variabel teks.</li>
<li>Program mencoba mendapatkan informasi bahasa dari header respons menggunakan response.headers.get("content-language"). Jika informasi bahasa tidak tersedia, program akan menggunakan bahasa Inggris ("en") sebagai bahasa default.</li>
<li>Speech synthesis dibuat menggunakan gTTS(teks, lang=bahasa), dengan teks dan bahasa yang telah ditentukan sebelumnya.</li>
<li>Hasil speech synthesis disimpan dalam file "output.mp3" menggunakan .save("output.mp3").</li></ul>

4. Program memeriksa argumen yang diberikan dari command-line. Jika argumen kurang dari 2 (hanya argumen pertama yaitu nama program), program mencetak pesan penggunaan dan keluar dengan status 1.

5. URL halaman web diambil dari argumen kedua (sys.argv[1]), dan fungsi speech_synthesis_from_web(url) dipanggil dengan URL tersebut untuk melakukan speech synthesis dari halaman web.
Dengan menjalankan program ini melalui command-line interface dengan memberikan URL sebagai argumen, program akan melakukan speech synthesis dari teks yang diekstraksi dari halaman web dan menyimpannya dalam file audio "output.mp3".


Demonstrasi Youtube: https://youtu.be/dKP4Hv0gwLo


# No 6 Artificial Intelligence Aplikasi
Speech Synthesis membaca artikel website otomatis ini menggunakan beberapa teknik kecerdasan buatan, terutama dalam pemrosesan teks dan sintesis suara. Berikut adalah beberapa aspek kecerdasan buatan yang terkait dengan program tersebut:

1. Web Scraping: Program menggunakan library BeautifulSoup untuk melakukan parsing (ekstraksi) konten dari halaman web. Ini melibatkan analisis struktur HTML, mengidentifikasi elemen yang relevan (misalnya, elemen paragraf) dan mengambil teks dari elemen tersebut. Ini adalah contoh pemrosesan teks dan pemahaman struktur yang dilakukan oleh program.
2. NLP (Natural Language Processing): Program mengolah teks yang diekstraksi dari halaman web. Dalam contoh ini, program menggabungkan teks dari beberapa elemen paragraf menjadi satu teks yang akan diucapkan oleh sistem speech synthesis. Hal ini melibatkan pemrosesan teks, seperti penggabungan dan pemisahan kata, pemrosesan frasa, dan penghapusan karakter atau simbol yang tidak diinginkan. Program juga mengenali informasi bahasa dari header respons, yang merupakan contoh pemrosesan metadata terkait teks.
3. Speech Synthesis: Program menggunakan library gTTS (Google Text-to-Speech) untuk mengubah teks menjadi suara. Ini melibatkan teknik sintesis suara, di mana teks dikonversi menjadi suara yang dapat didengar. gTTS mengandalkan teknik Text-to-Speech (TTS) yang telah dikembangkan dengan menggunakan metode Machine Learning dan Deep Learning untuk menghasilkan suara yang alami dan menyesuaikan suara dengan bahasa yang diinginkan.
4. Pengambilan Keputusan: Program membuat beberapa keputusan dalam alur kerjanya. Misalnya, jika informasi bahasa tidak tersedia dalam header respons, program akan menggunakan bahasa Inggris sebagai bahasa default. Selain itu, program juga memeriksa argumen yang diberikan melalui command-line dan memberikan pesan penggunaan jika argumen tidak sesuai. Pengambilan keputusan seperti ini melibatkan logika dan pemrosesan kondisional yang merupakan bagian dari kecerdasan buatan.
