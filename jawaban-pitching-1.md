# No 1 Dokumentasi dan Penggunaan Aplikasi

![Source Code](https://gitlab.com/deta.triandini/praksismul-uas/-/blob/master/compresVideo.py) <br>
![ScreenRecord](https://gitlab.com/deta.triandini/praksismul-uas/-/raw/master/screenrecord_video.gif)

**Tampilan Aplikasi**
1. Aplikasi dijalankan melalui command prompt atau CLI dengan user memasukkan perintah python namafile.py
![](https://gitlab.com/deta.triandini/praksismul-uas/-/raw/master/Tampilan%20Aplikasi/Tampilan-1.jpg)
2. Lalu program akan membuka jendela file dialog menggunakan tkinter (Membuka File explorer user)
![](https://gitlab.com/deta.triandini/praksismul-uas/-/raw/master/Tampilan%20Aplikasi/Tampilan-2.jpg)
3. Lalu user memilih file video yang akan di pangkas
![](https://gitlab.com/deta.triandini/praksismul-uas/-/raw/master/Tampilan%20Aplikasi/Tampilan-3.jpg)
4. User akan diminta memasukkan waktu untuk memulai video dalam satuan detik
![](https://gitlab.com/deta.triandini/praksismul-uas/-/raw/master/Tampilan%20Aplikasi/Tampilan-4.jpg)
5. User akan diminta memasukkan waktu untuk mengakhiri video dalam satuan detik
![](https://gitlab.com/deta.triandini/praksismul-uas/-/raw/master/Tampilan%20Aplikasi/Tampilan-5.jpg)
6. Aplikasi akan memproses pemangkasan klip video dari waktu mulai sampai waktu akhir video yang sudah diinputkan sebelumnya.
![](https://gitlab.com/deta.triandini/praksismul-uas/-/raw/master/Tampilan%20Aplikasi/Tampilan-6.jpg)
7. Proses pemangkasan selesai. Lalu hasil pemangkasan disimpan dengan nama output_video.mp4
![](https://gitlab.com/deta.triandini/praksismul-uas/-/raw/master/Tampilan%20Aplikasi/Tampilan-7.jpg)
8. Setelah proses pemangkasan video selesai, dilakukan konversi dari file video menjadi audio. (.mp4 menjadi .mp3)
![](https://gitlab.com/deta.triandini/praksismul-uas/-/raw/master/Tampilan%20Aplikasi/Tampilan-9.jpg)

Berikut adalah beberapa contoh penggunaan (use cases) untuk aplikasi ini:

1. Pemotongan Klip Video: Pengguna dapat menggunakan aplikasi ini untuk memotong klip video yang panjang menjadi klip yang lebih pendek. Misalnya, pengguna ingin memotong klip video pernikahan menjadi klip hanya dengan momen tertentu seperti upacara pengucapan janji atau pertunjukan tari. Dengan menggunakan aplikasi ini, mereka dapat memilih video tersebut, memasukkan waktu mulai dan waktu akhir yang diinginkan, dan menghasilkan klip video yang dipotong sesuai dengan rentang waktu yang diminta.

2. Ekstraksi Audio dari Video: Aplikasi ini dapat digunakan untuk mengekstrak audio dari video yang ada. Misalnya, jika pengguna memiliki file video musik favorit, mereka dapat menggunakan aplikasi ini untuk mengekstrak audio dari video tersebut dan menyimpannya sebagai file audio terpisah. Ini berguna jika pengguna ingin mendengarkan musik di perangkat audio yang tidak mendukung format video atau jika mereka hanya ingin mendengarkan audio tanpa video yang menyertainya.

3. Konversi Video ke Format Audio MP3: Aplikasi ini memungkinkan pengguna untuk mengonversi video ke format audio MP3. Pengguna dapat memilih video favorit mereka dan mengubahnya menjadi file audio MP3 yang dapat diputar di perangkat audio atau aplikasi pemutar musik. Ini berguna jika pengguna ingin mendengarkan audio video di perangkat yang hanya mendukung format audio atau jika mereka ingin menghemat ruang penyimpanan dengan mengubah video menjadi format audio yang lebih kecil.

4. Pengeditan Audio: Setelah mengonversi video ke audio, pengguna dapat menggunakan aplikasi ini untuk melakukan pengeditan sederhana pada audio tersebut. Misalnya, mereka dapat memangkas bagian awal atau akhir audio, menyesuaikan volume, atau menambahkan efek suara. Ini memungkinkan pengguna untuk melakukan penyesuaian pada audio agar sesuai dengan kebutuhan mereka sebelum menyimpannya sebagai file audio terpisah.

5. Otomatisasi: Aplikasi ini juga dapat digunakan dalam konteks otomatisasi, misalnya dalam skenario di mana pengguna memiliki banyak video yang perlu dipotong atau dikonversi ke format audio. Dalam hal ini, pengguna dapat menggunakan aplikasi ini untuk mengotomatiskan proses pemotongan dan konversi video ke audio, dengan mengatur skrip atau skenario yang sesuai dengan kebutuhan mereka.

Dalam semua use cases ini, aplikasi ini memberikan fleksibilitas dan kemudahan penggunaan untuk memanipulasi video dan audio sesuai kebutuhan pengguna.

# No 2 Cara Kerja
Produk video yang saya buat adalah aplikasi berbasis desktop/CLI. Program ini merupakan aplikasi sederhana untuk memangkas video berdasarkan waktu yang ditentukan oleh pengguna dan mengonversi format file menjadi audio. Aplikasi ini bertujuan untuk memudahkan pengguna dalam memotong video dengan menggunakan interaksi pengguna melalui GUI (Graphical User Interface) yang disediakan oleh library tkinter. Dengan memilih file video, memasukkan waktu mulai dan waktu akhir, program akan memotong video sesuai dengan input pengguna, kemudian dikonversi menjadi file audio MP3.

Penjelasan Program :

Program di atas berfungsi untuk memangkas video dan mengonversinya ke format audio MP3. Berikut adalah cara kerja program tersebut secara keseluruhan:

1. Program akan membuka jendela dialog menggunakan library `tkinter` yang memungkinkan pengguna memilih file video (*.mp4) dari sistem file mereka melalui fungsi `select_video_file()`. Path file video yang dipilih akan disimpan dalam variabel `input_path`.

2. Pengguna diminta untuk memasukkan waktu mulai dan waktu akhir dalam detik melalui fungsi `input()`. Waktu tersebut akan disimpan dalam variabel `start_time` dan `end_time`.

3. Fungsi `trim_video()` dipanggil dengan argumen `input_path`, `output_path`, `start_time`, dan `end_time`. Fungsi ini akan memangkas video berdasarkan rentang waktu yang dimasukkan oleh pengguna menggunakan library `moviepy`. Video dipotong menggunakan `VideoFileClip` dan hasil pemangkasan disimpan dalam file video output yang ditentukan oleh `output_path`.

4. Setelah video dipangkas dan disimpan, fungsi `convert_to_mp3()` dipanggil dengan argumen `output_path` dan `output_mp3_path`. Fungsi ini mengonversi video yang telah dipangkas menjadi format audio MP3. Video diambil menggunakan `VideoFileClip`, dan objek audio dari video tersebut diekstrak. Objek audio kemudian disimpan sebagai file audio output dengan menggunakan metode `write_audiofile` dari objek audio.

5. Program selesai dan file video yang dipangkas tersimpan sebagai file video output sesuai dengan `output_path`, serta file audio MP3 tersimpan sebagai file audio output sesuai dengan `output_mp3_path`.

Secara keseluruhan, program ini memungkinkan pengguna memilih file video, memasukkan waktu mulai dan waktu akhir, dan memotong video berdasarkan input tersebut. Hasil pemotongan video disimpan sebagai file output yang ditentukan. Setelah video dipangkas dilakukan konversi format file menjadi mp3.Program menggunakan library seperti `cv2`, `tkinter`, dan `moviepy.editor` untuk memanipulasi video dan memungkinkan interaksi dengan pengguna melalui GUI.

<b>Berikut Flowchart Aplikasi</b>
``` mermaid 
    flowchart TB
        id1([Start])-->id2[A dialog window appears to select a video file]
        id2-->id3[/User selects a video file/]
        id3-->id4[Save selected video file path]
        id4-->id5[Set the output file path for the trimmed video]
        id5-->id6[/user is prompted to enter a start time/]
        id6-->id7[/user is prompted to enter a end time/]
        id7-->id8[Trim video clip from start time to end time]
        id8-->id9[Save trimmed video to output file]
        id9-->id10[convert trimmed video to audio]
        id10-->id11[Save audio to output file]
        id11-->id12([End])
```
<b>Flowchart convert Audio from Video file</b>
```mermaid
    flowchart TB
        id1([Start])-->id2[Load video file]
        id2-->id3[Load video file]
        id3-->id4[Create VideoFileClip object]
        id4-->id5[Extract audio from VideoFileClip]
        id5-->id6[Set output file format and parameters]
        id6-->id7[Save audio as file]
        id7-->id8([End])
```
<br>
<b>Berikut Demonstrasi Aplikasi: </b>https://youtu.be/A6QC9Eizy04
